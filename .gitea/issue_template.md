## # What do you want to address?
<!-- This step is required; examples are shown below -->

- [ ] Bug
- [ ] Feature
- [ ] Suggestion

## # Describe your matter briefly
<!-- This step is required. -->
<br><br>

##### What did you expect? <!-- Useful when addressing bugs -->
---
<!-- This step is optional. -->
<br><br>

##### Some additional details <!-- Useful, when we are trying to reproduce a bug -->
---
<!-- This step is optional; an example is shown below -->

* The version of **Gitea** you are using: 
* The version of **GitNex** you are using: 
* Source of installation (Play Store, F-Droid, APK): 
* Current android version and phone model/manufacturer: 
* The type of certificate your instance is using (self-signed, signed): 
* How you used to log in (via password or token): 
<br>

##### We would appreciate some screenshots or stacktrace's, but this is also not required.
---
<!-- Screenshots and stacktrace's can go here. -->
<br><br>

- [ ] I carefully read the [contribution guidelines](https://codeberg.org/GitNex/GitNex/src/branch/main/CONTRIBUTING.md).
<br>

#### Thank you for your time.